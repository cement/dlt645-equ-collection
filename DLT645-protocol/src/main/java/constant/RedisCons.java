package constant;

/**
 * redis键-常量类（防止redis键冲突）
 * 
 * @author ack @date Oct 19, 2019
 *
 */
public class RedisCons {

	/**
	 * 电表地址域(key构成：电表client的ip+":"+client端口) EX:address_192.168.199.236:9668
	 */
	public static final String address = "address_";

	/**
	 * 设备采集key前缀（key构成：一级key:采集server的ip+":"+server端口+电表client的ip+":"+client端口）
	 */
	public static final String collect = "collect_S:";

}
