/**
 *
 */
package com.ems.vo;


public class PageVo extends BaseVO{


	public static final String ASC = "ASC";
	public static final String DESC = "DESC";

	/** 分页 */
	//当前页码
	private Integer page;
	//每页条数
	private Integer rows;
	//总条目
	private Integer total;
	//排序
	private String orderBy;
	//升序：asc;降序desc
	private String sort;


	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public static String getASC() {
		return ASC;
	}

	public static String getDESC() {
		return DESC;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
}
